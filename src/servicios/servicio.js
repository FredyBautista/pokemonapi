import axios from 'axios';

const API = "https://pokeapi.co/api/v2/pokemon/";

export async function getPokemon(namePokemon){
    try {
        const pokemon = await axios.get(`${API}${namePokemon}/`);
        const {id, name, sprites} = pokemon.data;
        return {
            id,
            name,
            picture: sprites.front_default
        }
    }
    catch(err){
        console.error(`Can not find pokemon ${namePokemon}`, err);
    }
}

export default getPokemon;