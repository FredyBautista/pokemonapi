import React, {Component} from 'react';
import './App.css';
import 'antd/dist/antd.css';
import { Input, Button, Row, Col, List, Avatar } from 'antd';
import {getPokemon} from './servicios/servicio';

export class App extends Component {
  state = {
      namePokemon: '',
      listPokemon: []
  }

  onChange = (e) => {
    this.setState({
      namePokemon: e.target.value
    });
  }

  searchPokemon = () =>{
    const {namePokemon} = this.state; 
    getPokemon(namePokemon).then( pokemon => {
      if(pokemon){
        this.setState(state => {
          return { listPokemon: state.listPokemon.concat(pokemon) }
        })
      }
    })
    .catch(err => console.error(err));
  }

  render(){
    return (
      <Row className="App">
        <header className="App-header">               
          <Col span={8}><Input colplaceholder="Type name of Pokémon" allowClear onChange={this.onChange} /></Col>
          <Col span={8}><Button className="catch" type="primary" onClick={this.searchPokemon}>Catch</Button></Col>
          <div className="list">
            <List
              itemLayout="horizontal"
              dataSource={this.state.listPokemon}
              renderItem={item => (
                <List.Item>
                  <List.Item.Meta
                    avatar={<Avatar size={80} src={item.picture} />}
                    title={`PokemonId (Number): ${item.id}`}
                    description={`Pokemon Name: ${item.name}`}
                  />
                </List.Item>
              )}
            />
          </div>     
        </header>
      </Row>
    );
  }
}

export default App;
