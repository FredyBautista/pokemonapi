import React from 'react';
import App from './App';
import {shallow, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {getPokemon} from './servicios/servicio';

configure({ adapter: new Adapter() })

describe("renders without crashing", () => {
  test('render app', ()=> {
    shallow(<App />);
  });
});

describe("catch button", () => {
  it('size of the list would be 1 when the catch button is clicked', () => {
    const wrapper = shallow(<App />);
    const catchBtn = wrapper.find('.catch');
    catchBtn.simulate('click');
    const length = wrapper.find('.list').length;
    expect(length).toEqual(1);
  });
});

describe("Testing the API call", () => {
  it('calling the getPokemon (service)', async () => {
    const pokemon = await getPokemon('charmander');
    expect(pokemon.id).toEqual(4);
  });
});